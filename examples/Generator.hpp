#pragma once
#include "systemc.h"

class Generator: public sc_module {
public:
	sc_fifo_out<double> out;
	int i;

private:
	int max;
	void produce() {
		for (i=1; i<=max; i++) {
			out.write(i);
			cout << "generator: " << i << std::endl;
		}
	}

public:
	SC_HAS_PROCESS(Generator);
	Generator(sc_module_name name, int m)
	: sc_module(name), max(m) {
		SC_THREAD(produce);
	}
};
