#pragma once
#include "systemc.h"

class DUV: public sc_module {
public:
	sc_fifo_in<double>  in;
	sc_fifo_out<double> out;
	double data;

private:
	void pass() {
		while(true) {
			data = in.read();
			std::cout << "duv:  " << data << std::endl;
			out.write(data);
		}
	}

public:
	SC_HAS_PROCESS(DUV);

	DUV(sc_module_name name) : sc_module(name) {
		SC_THREAD(pass);
	}
};
