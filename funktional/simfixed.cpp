#include "EquationSolverTop.hpp"

#define SC_INCLUDE_FX
#include <systemc.h>
#include <iostream>

int sc_main(int, char**) {
  /*********************************************************
   * Instantiiere das Top-Modul mit dem gegebenen Datentyp *
   *********************************************************/
  EquationSolverTop<...> top("top", "simfixed.data");

  // Startet die Simulation
  sc_start();
  return 0;
}

