#pragma once

#define SC_INCLUDE_FX
#include <systemc.h>
#include <fstream>

template<class T>
class EquationSolverTest : public sc_module{
public:
  sc_out<T> x0_out;
  sc_out<T> u0_out;
  sc_out<T> y0_out;
  sc_out<T> dx_out;
  sc_out<T> a_out;
  sc_in<T> y_in;

  sc_in<bool> ready;
  sc_out<bool> start;

  EquationSolverTest(sc_module_name name, const char* datafile)
    : sc_module(name),
      data(datafile)
  {
    SC_THREAD(go);
  }

private:
  SC_HAS_PROCESS(EquationSolverTest);
  std::ofstream data;

  void go() {
    x0_out = 0;
    u0_out = 1;
    y0_out = 0;
    dx_out = 0.1;

    for(T a = 0; a < 10; a += 0.1) {    

      // Wait until 'ready' goes low again
      if(ready)
        wait(ready.negedge_event());

      // Set initial values
      a_out = a;

      // Start computation and wait for the results
      start = true;

      wait(ready.posedge_event());

      start = false;

      data << a << " " << y_in << std::endl;
    }
  }
};
