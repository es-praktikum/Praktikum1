#pragma once

#define SC_INCLUDE_FX
#include <systemc.h>

template<class T>
class EquationSolver : public sc_module{
public:
  sc_in<T> x0_in;
  sc_in<T> u0_in;
  sc_in<T> y0_in;
  sc_in<T> dx_in;
  sc_in<T> a_in;
  sc_out<T> y_out;

  sc_in<bool> start;
  sc_out<bool> ready;

  EquationSolver(sc_module_name name)
    : sc_module(name)
  {
    SC_THREAD(go);
  }

private:
  SC_HAS_PROCESS(EquationSolver);

  void go() {

    while(true) {
      // Warte auf positive Taktflanke von 'start'
      wait(start.posedge_event());

      /****************************************
       *     Approximiere den Wert von y      *
       ****************************************/

        ...


      /****************************************
       * Schreibe berechneten Wert nach y_out *
       ****************************************/
      y_out = ...;

      // Positive Taktflanke von 'ready' zeigt Abschluss der Berechnung
      ready = true;

      // Warte auf negative Taktflanke von 'start'
      wait(start.negedge_event());

      // 'ready' zurücksetzen
      ready = false;
    }
  }
};
