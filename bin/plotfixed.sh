#!/bin/bash

./simfixed
echo "set terminal canvas size 1024,768 rounded standalone mousing jsdir \"http://gnuplot.sourceforge.net/demo_canvas\"; set output 'plotfixed.html'; plot [0:10] [-0.1:0.6] 'simfixed.data' smooth unique with linespoints" > simfixed.plot_html
echo "set terminal png size 1024,768; set output 'plotfixed.png'; plot [0:10] [-0.1:0.6] 'simfixed.data' smooth unique with linespoints" > simfixed.plot

gnuplot simfixed.plot
gnuplot simfixed.plot_html
