#!/bin/bash

./simfloat

echo "set terminal canvas size 1024,768 rounded standalone mousing jsdir \"http://gnuplot.sourceforge.net/demo_canvas\"; set output 'plotfloat.html'; plot [0:10] [-0.1:0.6] 'simfloat.data' smooth unique with linespoints" > simfloat.plot_html
echo "set terminal png size 1024,768; set output 'plotfloat.png'; plot [0:10] [-0.1:0.6] 'simfloat.data' smooth unique with linespoints" > simfloat.plot

gnuplot simfloat.plot
gnuplot simfloat.plot_html